#pragma once

#include <dnlib/mslib.hpp>
#include <dnlib/unique_releasable.hpp>
#include <string>

namespace dnlib
{
	class runtime_info : public unique_releasable<mslib::ICLRRuntimeInfo>
	{
	public:
		runtime_info() = default;
		runtime_info(mslib::ICLRRuntimeInfo * i);

		auto get_version_string() -> std::wstring;

		static auto from_unknown(const unique_releasable<mslib::IUnknown> & i) -> runtime_info;
	};
}