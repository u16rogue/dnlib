#pragma once

#include <dnlib/unique_releasable.hpp>
#include <dnlib/mslib.hpp>

namespace dnlib
{
	class clr_debugging : public unique_releasable<mslib::ICLRDebugging>
	{
	public:
		clr_debugging() = default;
		clr_debugging(mslib::ICLRDebugging * i);

		static auto create_instance() -> clr_debugging;
	private:
	};
}