#pragma once

#ifdef _WIN32
	#include <Windows.h>
#elif linux
	#error "Currently unsupported"
#else
	#error "Unsupported platform"
#endif

namespace dnlib::impl
{
	/*
	struct base_loader
	{
		using hmod_t = void;
		static auto load_module(const char * n) -> hmod_t;
		static auto proc_module(hmod_t m, const char * n) -> void *;
	};
	*/

	#ifdef _WIN32
		template <typename T>
		auto clr_create_instance(const dnlib::GUID & clsid, const dnlib::GUID & riid) -> T *
		{
			void * res = nullptr;
			static HMODULE hmscoree = nullptr;

			if (!hmscoree)
				hmscoree = LoadLibraryW(L"MSCorEE.dll");

			if (hmscoree)
			{ 
				long(__stdcall * _CLRCreateInstance)(const dnlib::GUID &, const dnlib::GUID &, void **) = reinterpret_cast<decltype(_CLRCreateInstance)>(GetProcAddress(hmscoree, "CLRCreateInstance"));
				if (_CLRCreateInstance)
					_CLRCreateInstance(clsid, riid, &res);
			}

			return reinterpret_cast<T *>(res);
		}
	#elif linux
		#error "Linux build does not provide a default loader"
	#endif
}