#pragma once

// Library implementation of microsoft SDK and libraries for dotNet

#include <Windows.h>

//#include <mscoree.h>
//#include <cordebug.h>

namespace dnlib
{
    namespace impl
    {
        
    }

	struct GUID
	{
		unsigned long  Data1;
		unsigned short Data2;
		unsigned short Data3;
		unsigned char  Data4[8];
	
		// [13/06/2022] This is trivial to compare which doesn't need this operator overload but... 
		auto operator==(const GUID & rhs) const noexcept -> bool
		{
            return Data1    == rhs.Data1
                && Data2    == rhs.Data2
                && Data3    == rhs.Data3
                && Data4[0] == rhs.Data4[0]
                && Data4[1] == rhs.Data4[1]
                && Data4[2] == rhs.Data4[2]
                && Data4[3] == rhs.Data4[3]
                && Data4[4] == rhs.Data4[4]
                && Data4[5] == rhs.Data4[5]
                && Data4[6] == rhs.Data4[6]
                && Data4[7] == rhs.Data4[7];
		}

        constexpr operator bool() const noexcept
        {
			return Data1    
                && Data2    
                && Data3    
                && Data4[0] 
                && Data4[1] 
                && Data4[2] 
                && Data4[3] 
                && Data4[4] 
                && Data4[5] 
                && Data4[6] 
                && Data4[7]; 
        }
	};
}

namespace dnlib::mslib
{
	class IUnknown
	{
	public:	
		virtual long __stdcall QueryInterface(const GUID & riid, void * ppvObject) = 0;
		virtual unsigned long __stdcall AddRef(void) = 0;
		virtual unsigned long __stdcall Release(void) = 0;
	};

	class IEnumUnknown: public IUnknown
    {
    public:
        virtual /* [local] */ long __stdcall Next(
            /* [annotation][in] */
		    unsigned long celt,
            /* [annotation][out] */
            IUnknown **rgelt,
            /* [annotation][out] */
            unsigned long *pceltFetched) = 0;

        virtual long __stdcall Skip(
            /* [in] */ unsigned long celt) = 0;

        virtual long __stdcall Reset(void) = 0;

        virtual long __stdcall Clone(
            /* [out] */ IEnumUnknown ** ppenum) = 0;

    };

	class ICLRDebugging : public IUnknown
    {
    public:
        virtual long __stdcall OpenVirtualProcess( 
            /* [in] */ unsigned long long moduleBaseAddress,
            /* [in] */ IUnknown *pDataTarget,
            /* [in] */ /* ICLRDebuggingLibraryProvider */ void *pLibraryProvider,
            /* [in] */ /* CLR_DEBUGGING_VERSION */ void *pMaxDebuggerSupportedVersion,
            /* [in] */ const GUID & riidProcess,
            /* [iid_is][out] */ IUnknown **ppProcess,
            /* [out][in] */ /* CLR_DEBUGGING_VERSION */ void *pVersion,
            /* [out] */ /* CLR_DEBUGGING_PROCESS_FLAGS */ void *pdwFlags) = 0;
        
        virtual long __stdcall CanUnloadNow( 
            void * hModule) = 0;
    };

    class ICLRMetaHost: public IUnknown
    {
    public:
        virtual long __stdcall GetRuntime(
            /* [in] */ const wchar_t * pwzVersion,
            /* [in] */ const GUID & riid,
            /* [retval][iid_is][out] */ void ** ppRuntime) = 0;

        virtual long __stdcall GetVersionFromFile(
            /* [in] */ const wchar_t * pwzFilePath,
            /* [annotation][size_is][out] */
						const wchar_t * pwzBuffer,
            /* [out][in] */ unsigned long * pcchBuffer) = 0;

        virtual long __stdcall EnumerateInstalledRuntimes(
            /* [retval][out] */ IEnumUnknown ** ppEnumerator) = 0;

        virtual long __stdcall EnumerateLoadedRuntimes(
            /* [in] */ void * hndProcess,
            /* [retval][out] */ IEnumUnknown ** ppEnumerator) = 0;

        virtual long __stdcall RequestRuntimeLoadedNotification(
            /* [in] */ void * pCallbackFunction) = 0;

        virtual long __stdcall QueryLegacyV2RuntimeBinding(
            /* [in] */ const GUID & riid,
            /* [retval][iid_is][out] */ void ** ppUnk) = 0;

        virtual long __stdcall ExitProcess(
            /* [in] */ int ExitCode) = 0;

    };

    class ICLRRuntimeInfo : public IUnknown
    {
    public:
        virtual long __stdcall GetVersionString(
            /* [annotation][size_is][out] */
            const wchar_t *pwzBuffer,
            /* [out][in] */ unsigned long *pcchBuffer) = 0;

        virtual long __stdcall GetRuntimeDirectory(
            /* [annotation][size_is][out] */
            const wchar_t *pwzBuffer,
            /* [out][in] */ unsigned long * pcchBuffer) = 0;

        virtual long __stdcall IsLoaded(
            /* [in] */ void *  hndProcess,
            /* [retval][out] */ int * pbLoaded) = 0;

        virtual long __stdcall LoadErrorString(
            /* [in] */ int iResourceID,
            /* [annotation][size_is][out] */
            const wchar_t *pwzBuffer,
            /* [out][in] */ unsigned long * pcchBuffer,
            /* [lcid][in] */ long iLocaleID) = 0;

        virtual long __stdcall LoadLibrary(
            /* [in] */ const wchar_t * pwzDllName,
            /* [retval][out] */ void *  * phndModule) = 0;

        virtual long __stdcall GetProcAddress(
            /* [in] */ const char * pszProcName,
            /* [retval][out] */ void * * ppProc) = 0;

        virtual long __stdcall GetInterface(
            /* [in] */ const GUID & rclsid,
            /* [in] */ const GUID & riid,
            /* [retval][iid_is][out] */ void * * ppUnk) = 0;

        virtual long __stdcall IsLoadable(
            /* [retval][out] */ int * pbLoadable) = 0;

        virtual long __stdcall SetDefaultStartupFlags(
            /* [in] */ unsigned long dwStartupFlags,
            /* [in] */ const wchar_t *  pwzHostConfigFile) = 0;

        virtual long __stdcall GetDefaultStartupFlags(
            /* [out] */ unsigned long * pdwStartupFlags,
            /* [annotation][size_is][out] */
            const wchar_t *pwzHostConfigFile,
            /* [out][in] */ unsigned long * pcchHostConfigFile) = 0;

        virtual long __stdcall BindAsLegacyV2Runtime(void) = 0;

        virtual long __stdcall IsStarted(
            /* [out] */ int * pbStarted,
            /* [out] */ unsigned long * pdwStartupFlags) = 0;

    };

    class ICorRuntimeHost: public IUnknown
    {
    public:
        virtual long __stdcall CreateLogicalThreadState(void) = 0;

        virtual long __stdcall DeleteLogicalThreadState(void) = 0;

        virtual long __stdcall SwitchInLogicalThreadState(
            /* [in] */ unsigned long * pFiberCookie) = 0;

        virtual long __stdcall SwitchOutLogicalThreadState(
            /* [out] */ unsigned long ** pFiberCookie) = 0;

        virtual long __stdcall LocksHeldByLogicalThread(
            /* [out] */ unsigned long * pCount) = 0;

        virtual long __stdcall MapFile(
            /* [in] */ void * hFile,
            /* [out] */ void ** hMapAddress) = 0;

        virtual long __stdcall GetConfiguration(
            /* [out] */ void ** pConfiguration) = 0;

        virtual long __stdcall Start(void) = 0;

        virtual long __stdcall Stop(void) = 0;

        virtual long __stdcall CreateDomain(
            /* [in] */ const wchar_t * pwzFriendlyName,
            /* [in] */ IUnknown * pIdentityArray,
            /* [out] */ IUnknown ** pAppDomain) = 0;

        virtual long __stdcall GetDefaultDomain(
            /* [out] */ IUnknown ** pAppDomain) = 0;

        virtual long __stdcall EnumDomains(
            /* [out] */ void ** hEnum) = 0;

        virtual long __stdcall NextDomain(
            /* [in] */ void * hEnum,
            /* [out] */ IUnknown ** pAppDomain) = 0;

        virtual long __stdcall CloseEnum(
            /* [in] */ void * hEnum) = 0;

        virtual long __stdcall CreateDomainEx(
            /* [in] */ const wchar_t * pwzFriendlyName,
            /* [in] */ IUnknown * pSetup,
            /* [in] */ IUnknown * pEvidence,
            /* [out] */ IUnknown ** pAppDomain) = 0;

        virtual long __stdcall CreateDomainSetup(
            /* [out] */ IUnknown ** pAppDomainSetup) = 0;

        virtual long __stdcall CreateEvidence(
            /* [out] */ IUnknown ** pEvidence) = 0;

        virtual long __stdcall UnloadDomain(
            /* [in] */ IUnknown * pAppDomain) = 0;

        virtual long __stdcall CurrentDomain(
            /* [out] */ IUnknown ** pAppDomain) = 0;
    };
}