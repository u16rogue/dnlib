#pragma once

#include <dnlib/unique_releasable.hpp>
#include <dnlib/mslib.hpp>

#include <dnlib/meta_host.hpp>
#include <dnlib/runtime_info.hpp>
#include <dnlib/clr_debugging.hpp>

namespace dnlib
{
	auto clr_create_instance(const dnlib::GUID & clsid, const dnlib::GUID & riid) -> unique_releasable<mslib::IUnknown>;
}