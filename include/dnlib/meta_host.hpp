#pragma once

#include <dnlib/unique_releasable.hpp>
#include <dnlib/mslib.hpp>
#include <dnlib/next_iterator.hpp>
#include <dnlib/runtime_info.hpp>

namespace dnlib
{
	class meta_host : public unique_releasable<mslib::ICLRMetaHost>
	{
	public:
		meta_host() = default;
		meta_host(mslib::ICLRMetaHost * i);

		static auto create_instance() -> meta_host;

		auto enumerate_loaded_runtimes(void * handle) -> next_iterator<runtime_info>;
	private:
	};
}