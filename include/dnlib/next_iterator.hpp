#pragma once

#include <dnlib/mslib.hpp>
#include <dnlib/unique_releasable.hpp>

namespace dnlib
{
	// [16/06/2022] This is intended to be used for a range loop only
	template <typename T, dnlib::GUID T_guid = dnlib::GUID { 0 }>
	class next_iterator
	{
		// [20/06/2022] from_unknown's parameter must be implemented as a rvalue reference [from_unknown(const T &)] otherwise a move operation will clear current 
		static_assert(requires { T::from_unknown(unique_releasable<mslib::IUnknown>()); } || T_guid, "T must either implement a from_unknown static method or instance must be provided with a GUID with the T_guid template parameter");

	public:
		next_iterator(mslib::IEnumUnknown * i)
			: enumerator(i), count(0), current(nullptr)
		{}

		auto begin() -> next_iterator &&
		{
			if (enumerator && !count && !current)
				++*this;
			return std::move(*this);
		}

		auto end() -> next_iterator
		{
			return (next_iterator<T, T_guid>(nullptr));
		}

		auto operator++() -> void
		{
			mslib::IUnknown * c = nullptr;
			count = 0;
			enumerator->Next(1, &c, &count);
			current = c; 
		}

		auto operator*() -> T requires ([](){ if constexpr (requires { T::from_unknown(unique_releasable<mslib::IUnknown>()); }) return true; else return false; }()) // [20/06/2022] uhh, need to check if from_unknown 
		{																																							  // exists but by directly using it in a requires clause 
			return T::from_unknown(current);																														  // it instead checks if the call returns rather
		}																																							  // than checking if the call is possible in the first place 

		auto operator*() -> unique_releasable<T> requires ( !!T_guid )
		{
			T * r = nullptr;
			current->QueryInterface(T_guid, &r);
			return unique_releasable<T>(r);
		}

		auto operator!=(const next_iterator & rhs) -> bool
		{
			if (enumerator && !current)
				++*this;
			return current != rhs.current && enumerator != rhs.enumerator && count; 
		}
	private:
		unsigned long count = 0;
		unique_releasable<mslib::IEnumUnknown> enumerator;
		unique_releasable<mslib::IUnknown> current;
	};
}