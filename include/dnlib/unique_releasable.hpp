#pragma once

namespace dnlib
{
	template <typename T>
	class unique_releasable
	{
		// static_assert(sizeof(unique_releasable<T>) == sizeof(void*), "dnlib::unique_releasable is not its expected size!");
	public:
		// Disable copy
		unique_releasable(unique_releasable &)       = delete;
		unique_releasable(const unique_releasable &) = delete;

		unique_releasable()
			: instance(nullptr)
		{}

		~unique_releasable()
		{
			remove();
		}

		unique_releasable(T * i)
			: instance(i)
		{}

		// Move operation
		unique_releasable(unique_releasable && other)
			: instance(nullptr)
		{
			move(static_cast<unique_releasable &&>(other));
		}

		// Move operation
		auto operator = (unique_releasable && rhs) -> void
		{
			move(static_cast<unique_releasable &&>(rhs));
		}

		// Access instance members
		auto operator -> () const -> T *
		{
			return instance;
		}

		// Access the instance pointer as a reference
		// [16/06/2022] NOTE: The semantic does not mean it gives the dereferenced value of T* instance but rather references the instance member as a T*&
		//              tl;dr You wont get T from instance but instead you get T*& which is essentially the T* member
		auto operator * () -> T *&
		{
			return instance;
		}

		// Validation check
		operator bool() const noexcept
		{
			return instance && instance != reinterpret_cast<T*>(-1);
		}

		// Manual Virtual call wrapper
		template <int idx, typename R, typename... vargs_t>
		auto vcall(vargs_t... vargs) const -> R
		{
			using fn_t = R(__stdcall*)(T*, vargs_t...);
			return reinterpret_cast<fn_t**>(instance)[0][idx](instance, vargs...);
		}

		// Relinquish ownership without releasing
		auto forget() -> T *
		{
			T * i = instance;
			instance = nullptr;
			return i;
		}

	private:
		// Move implementation
		auto move(unique_releasable && other) -> void
		{
			remove();
			instance = other.instance;
			other.instance = nullptr;
		}

		// Removes the current instance
		auto remove() -> void
		{
			if (instance)
				release();
			instance = nullptr;
		}

		// Calls the Release of the current instance
		auto release() -> void 
		{
			if constexpr (requires { instance->Release(); })
				instance->Release();
			else
				vcall<2, long>();
		}

	protected:
		T * instance;
	};
}