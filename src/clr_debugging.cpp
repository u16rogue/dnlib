#include <dnlib/clr_debugging.hpp>
#include <dnlib/impl.hpp>

dnlib::clr_debugging::clr_debugging(mslib::ICLRDebugging * i)
	: unique_releasable<mslib::ICLRDebugging>(i)
{}

auto dnlib::clr_debugging::create_instance() -> clr_debugging
{
	constexpr dnlib::GUID CLSID_CLRDebugging = {
		.Data1 = 0xbacc578d,
		.Data2 = 0xfbdd,
		.Data3 = 0x48a4,
		.Data4 = { 0x96, 0x9f, 0x2, 0xd9, 0x32, 0xb7, 0x46, 0x34 }
	};

	constexpr dnlib::GUID IID_ICLRDebugging = {
		.Data1 = 0xd28f3c5a,
		.Data2 = 0x9634,
		.Data3 = 0x4206,
		.Data4 = { 0xa5, 0x9, 0x47, 0x75, 0x52, 0xee, 0xfb, 0x10 }
	};

	return clr_debugging(impl::clr_create_instance<mslib::ICLRDebugging>(CLSID_CLRDebugging, IID_ICLRDebugging));
}
