#include <dnlib/meta_host.hpp>
#include <dnlib/impl.hpp>

dnlib::meta_host::meta_host(mslib::ICLRMetaHost * i)
	: unique_releasable<mslib::ICLRMetaHost>(i)
{}

auto dnlib::meta_host::create_instance() -> meta_host
{
	constexpr dnlib::GUID CLSID_CLRMetaHost = {
		.Data1 = 0x9280188d,
		.Data2 = 0x0E8E,
		.Data3 = 0x4867,
		.Data4 = { 0xB3, 0x0C, 0x7F, 0xA8, 0x38, 0x84, 0xE8, 0xDE }
	};

	constexpr dnlib::GUID IID_ICLRMetaHost = {
		.Data1 = 0xD332DB9E,
		.Data2 = 0xB9B3,
		.Data3 = 0x4125,
		.Data4 = { 0x82, 0x07, 0xA1, 0x48, 0x84, 0xF5, 0x32, 0x16 }
	};

	return meta_host(impl::clr_create_instance<mslib::ICLRMetaHost>(CLSID_CLRMetaHost, IID_ICLRMetaHost));
}

auto dnlib::meta_host::enumerate_loaded_runtimes(void * handle) -> next_iterator<runtime_info>
{
	mslib::IEnumUnknown * ie = nullptr;
	instance->EnumerateLoadedRuntimes(handle, &ie);
	return next_iterator<runtime_info>(ie);
}
