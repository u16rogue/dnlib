#include <dnlib/runtime_info.hpp>

dnlib::runtime_info::runtime_info(mslib::ICLRRuntimeInfo * i)
	: unique_releasable<mslib::ICLRRuntimeInfo>(i)
{
}

auto dnlib::runtime_info::from_unknown(const unique_releasable<mslib::IUnknown> & i) -> runtime_info
{
	constexpr GUID IID_ICLRRuntimeInfo = {
		.Data1 = 0xBD39D1D2,
		.Data2 = 0xBA2F, 
		.Data3 = 0x486a,
		.Data4 = { 0x89, 0xB0, 0xB4, 0xB0, 0xCB, 0x46, 0x68, 0x91 }
	};

	mslib::ICLRRuntimeInfo * r = nullptr;
	i->QueryInterface(IID_ICLRRuntimeInfo, &r);
	return runtime_info(r);
}

auto dnlib::runtime_info::get_version_string() -> std::wstring
{
	wchar_t b[256] = {};
	unsigned long l = sizeof(b) / sizeof(wchar_t) - 1;
	instance->GetVersionString(b, &l);
	return std::wstring(b);
}