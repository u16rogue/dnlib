#include <dnlib/dnlib.hpp>
#include <dnlib/impl.hpp>

auto dnlib::clr_create_instance(const dnlib::GUID & clsid, const dnlib::GUID & riid) -> unique_releasable<mslib::IUnknown>
{
	return unique_releasable<mslib::IUnknown>(impl::clr_create_instance<mslib::IUnknown>(clsid, riid));
}